# Lol Discord Bot
Is a tool, to get endgame stats in you discord channel
it also provides ability to track records, achieved by your community. 

Picture with result

<img src="https://gitlab.com/kustikov112/lol-discord-bot/-/raw/main/readme_images/result.png" width=80% height=80%>


Record notification

<img src="https://gitlab.com/kustikov112/lol-discord-bot/-/raw/main/readme_images/record.png" width=30% height=30%>

Leaderboard

<img src="https://gitlab.com/kustikov112/lol-discord-bot/-/raw/main/readme_images/leaderboard.png" width=80% height=80%>


## How it works
If 2+ players from your "crew" table played a game together,
bot will get the data from that game and save stats,
mentioned in stats.yaml to database. After that it will compare the
previous result  of top score with current one. And if u hit the new record
it will generate picture and send it via webhook to your discord channel.

Also after that game it will generate "result picture" with stats
you mentioned in stats.yaml


## Installation
1. Clone the repo
2. rename TOKEN.yaml.example with TOKEN.yaml and fill it with valid tokens
3. Install docker
apt install docker.io
4. Install docker compose
mkdir -p ~/.docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
chmod +x ~/.docker/cli-plugins/docker-compose
5. run container with DB
docker-compose up db
6. go inside container 
docker exec -it lol_bot-db-1 /bin/sh
7. open psql 
psql -Ulol_db
8. Change table 
\c postgres
9. drop old database 
drop database lol_db;
10. Create clean database 
create database lol_db;
11. Restore data 
psql -Ulol_db -dlol_db -f PG_DUMP.sql

## Change theme of background
Background picture theme locates at 37 line of picture_generator.py

## Important
Records counting is working only for Classic mode in matched game. Its more competitive and mix it with ARAM, Custom games and event mode would be unfair.

Game result only saves and sends for games where 2+ crewmates were playing.
Its made for leave some privacy if you just tired of your mates and want to play solo a bit.

### TODO:
- Delete your server from Bot (To recreate it for example)
- Change channel 

### TOKENS
to make it work properly you need:
- Discord bot and token for him
- Webhook in the channel you want to send pictures
- RIOT api token





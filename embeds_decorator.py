import os
import random


emoji_list = [
    ':partying_face:',
    ':clown:',
    ':beers:',
    ':troll:',
    ':stuck_out_tongue_winking_eye:',
    ':disguised_face:',
    ':money_mouth:',
    ':cowboy:',
    ':love_you_gesture:',
    ':index_pointing_at_the_viewer:',
    ':man_in_tuxedo:',
    ':woman_elf:',
    ':man_mage:',
    ':woman_genie:',
    ':pregnant_person:',
    ':woman_in_motorized_wheelchair:',
    ':crown:',
    ':ring:',
    ':monkey_face:',
    ':unicorn:',
    ':lobster:',
    ':mushroom:',
    ':new_moon_with_face:',
    ':boom:',
    ':fire:',
    ':bone:',
    ':champagne_glass:',
    ':beers:',
    ':beer:',
    ':champagne:',
    ':sled:',
    ':women_wrestling:',
    ':military_medal:',
    ':video_game:',
    ':dart:',
    ':slot_machine:',
    ':accordion:',
    ':oncoming_police_car:',
    ':statue_of_liberty:',
    ':carousel_horse:',
    ':house_abandoned:',
    ':fireworks:',
    ':night_with_stars:',
    ':dollar:',
    ':money_with_wings:',
    ':yen:',
    ':euro:',
    ':pound:',
    ':coin:',
    ':moneybag:',
    ':credit_card:',
    ':gun:',
    ':bomb:',
    ':syringe:',
    ':pill:',
    ':test_tube:',
    ':abacus:',
    ':ok:',
    ':pirate_flag:',
    ':checkered_flag:'
]
def get_gambler_photo():
    path_to_folder = 'readme_images/mr_gambler'
    files = os.listdir(path_to_folder)
    random_gambler = random.choice(files)
    path_to_random_gambler = path_to_folder+'/'+random_gambler

    return random_gambler, path_to_random_gambler

def get_random_emoji():
    return random.choice(emoji_list)

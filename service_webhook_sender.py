from discord_webhook import DiscordWebhook, DiscordEmbed
import yaml


class SendServiceWebhookMessage:
    def __init__(self, cause):
        with open('TOKEN.yaml', 'r') as file_with_tokens:
            token = yaml.safe_load(file_with_tokens)    
        webhook_creds = token['service_webhook']
        self.webhook = DiscordWebhook(url=webhook_creds)
        self.cause = cause
        self.color_for_embed = {
            'new_server': '8fce00',
            'delete_server': 'cc0000',
            'new_summoner': '8fce00'
        }


    def send_notification(self, title, description):
        """Sending message via webhook to service discord channel

        Args:
            title (str): TItle of message
            description (str): message text
        """
        embed = DiscordEmbed(title=title, description=description, color=self.color_for_embed[self.cause])
        self.webhook.add_embed(embed)
        self.webhook.execute()
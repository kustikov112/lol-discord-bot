FROM python:3.11-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apk add --no-cache jpeg-dev zlib-dev g++ freetype-dev
RUN apk add --no-cache --virtual .build-deps build-base linux-headers \
    && pip install Pillow
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app 

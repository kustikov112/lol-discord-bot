import discord

try:
    import picture_generator
except ModuleNotFoundError:
    import sys
    sys.path.append("/usr/src/app")
finally:
    import picture_generator
    import service_webhook_sender
import yaml
import api_response
import postgres_requests
import regions
import embeds_decorator
# from discord.ext import commands


intents = discord.Intents.default()
intents.message_content = True
bot = discord.Bot(intents=intents)
stats_for_discord = {}
only_admin_restriction = 'Only administrator have permission to do that.\n' \
                         'Use /admins to call list of administrators'

with open('TOKEN.yaml', 'r') as f:
    token = yaml.safe_load(f)
with open('stats.yaml', 'r') as file:
    data = yaml.safe_load(file)

for key, values in data.items():
    try:
        stats_for_discord[values['discord_bot']] = key
    except KeyError:
        pass

gambler_photo_welldone = "gambler_master_welldone.png"
gambler_photo_wow = "gambler_master_wow.png"
gambler_photo_very_cool = "gambler_master_very_cool.png"
gambler_photo_sad_no_money = "gambler_master_sad_no_money.png"
gambler_photo_sad_cant_change = "gambler_master_sad_cant_change.png"
path_to_gambler = "readme_images/mr_gambler/"


def generate_summoners_list(message):
    summoners_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_list_of_summoners()
    position = 1
    string_of_summoners = ''
    for summoner in summoners_list:
        string_of_summoners += f'{position}. {summoner}\n'
        position += 1
    return string_of_summoners


def generate_summoners_options(message):
    list_of_options = []
    summoners_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_list_of_summoners()
    for summoner in summoners_list:
        list_of_options.append(discord.SelectOption(label=summoner))
    return list_of_options


def generate_games_options(message):
    list_of_games = []
    summoners_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_active_bets()
    for summoner in summoners_list:
        list_of_games.append(discord.SelectOption(label=summoner))
    return list_of_games


def generate_admins_options(message):
    list_of_options = []
    admins_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_admin_id_by_server()
    for admin in admins_list:
        list_of_options.append(discord.SelectOption(label=f'<@{admin}>'))
    return list_of_options


def generate_regions_options():
    list_of_options = []
    region_list = regions.RegionDecider().generate_region_list()
    for region in region_list:
        list_of_options.append(discord.SelectOption(label=region))
    return list_of_options


class RegionView(discord.ui.View):
    async def on_timeout(self):
        for child in self.children:
            child.disabled = True
        await self.message.edit(content="Timeout. Try Again", view=self)

    @discord.ui.select(
        placeholder="Chose your region",
        min_values=1,
        max_values=1,
        options=generate_regions_options()
    )
    async def select_callback(self, select, interaction):
        region = select.values[0]
        await interaction.channel.send(f"You chosen {region} region.")
        select.disabled = True
        await interaction.response.edit_message(view=self)
        await interaction.channel.send(f'Successfully added server to database')
        await interaction.channel.send(f"Now add summoners by using */add_summoner*")
        webhook = await interaction.channel.create_webhook(name='result')
        postgres_requests.PostgresRequest(interaction.guild.id).insert_new_server(interaction, webhook, region)


class DeletionDropdown(discord.ui.Select):
    def __init__(self, options, role, admins_list):
        self.role = role
        self.admins_list = admins_list
        super().__init__(
            placeholder=f"Choose {role} to delete",
            min_values=1,
            max_values=1,
            options=options,
        )

    async def callback(self, interaction):
        selection = self.values[0]
        self.disabled = True
        self.placeholder = selection
        await interaction.response.edit_message(view=self.view)
        self.view.stop()

        if self.role == 'summoner':
            postgres_requests.PostgresRequest(interaction.guild.id).delete_summoner(selection)
            await interaction.channel.send(f'Deleted {selection}')
        elif self.role == 'admin':
            discord_id = (selection.split('<')[-1])[1:-1]
            if (str(interaction.user.id)) == discord_id:
                await interaction.channel.send("you can't delete yourself ¯\_(ツ)_/¯")
                return
            self.admins_list.remove(discord_id)
            postgres_requests.PostgresRequest(interaction.guild.id).update_admins_list(self.admins_list)
            await interaction.channel.send(f"Deleted {selection}")


class DeletionView(discord.ui.View):
    def __init__(self, message, role, admins_list=None):
        super().__init__(timeout=20)
        if role == 'summoner':
            options = generate_summoners_options(message)
        elif role == 'admin':
            options = generate_admins_options(message)
        else:
            options = None
        self.add_item(DeletionDropdown(options, role, admins_list))

    async def on_timeout(self):
        for child in self.children:
            child.disabled = True
        await self.message.edit(content="Timeout.", view=self)


class AcceptanceView(discord.ui.View):  # Create a class called MyView that subclasses discord.ui.View
    def __init__(self):
        super().__init__(timeout=15)
        self.value = None

    async def on_timeout(self):
        for child in self.children:
            child.disabled = True
        await self.message.edit(content="Timeout", view=self)
        self.stop()

    @discord.ui.button(label="Yes", style=discord.ButtonStyle.primary)
    async def first_button_callback(self, button, interaction):
        self.value = True
        self.stop()
        for child in self.children:
            child.disabled = True
        await interaction.response.edit_message(view=self)

    @discord.ui.button(label="No", style=discord.ButtonStyle.primary)
    async def second_button_callback(self, button, interaction):
        self.value = False
        self.stop()
        for child in self.children:
            child.disabled = True
        await interaction.response.edit_message(view=self)



@bot.command(description="Start here")
async def start(message):
    # Trying to clean servers. If there is more when 2 months without any updates on server, it'll be deleted
    servers_to_delete = postgres_requests.PostgresRequest().get_list_of_servers_to_delete()
    if len(servers_to_delete) != 0:
        service_webhook_sender.SendServiceWebhookMessage('delete_server').send_notification('deleting server', str(servers_to_delete))
        for server in servers_to_delete:
            postgres_requests.PostgresRequest(server).delete_server_data()

    number_of_servers = postgres_requests.PostgresRequest(message.guild.id).get_number_of_servers()
    possible_number_of_servers = postgres_requests.PostgresRequest(message.guild.id).get_possible_number_of_servers()

    if number_of_servers >= possible_number_of_servers:
        await message.respond(f'Server limit is reached. Sorry. Try again later. Current limit is {number_of_servers}')
        return
    try:
        postgres_requests.PostgresRequest(message.guild.id).get_admin_id_by_server()
        await message.channel.send(f'Server already registered.\nContact your administrator.\n'
                                   f'*/admins* to show administrators list')
        return
    except TypeError:
        await message.respond('Select your region')
        await message.channel.send(view=RegionView(timeout=15))


@bot.command(description="HELP")
async def help_me(message):
    await message.respond("Type */leaderboard* to get full leaderBoard with all available stats.\n"
                               "*/summoners* to list all added summoners\n"
                               "*/delete summoner* to delete summoner\n"
                               "*/add_summoner* to add summoners\n"
                               "*/delete_summoner* to delete summoner\n"
                               "*/admins* to show bot's admins\n"
                               "*/add_admin* to add admin\n"
                               "*/delete_admin* to delete admin")


@bot.command(description="Add summoners")
async def add_summoner(
    message,
    summoner_name: discord.Option(str, description='summoner name in game'),
    summoner_tag: discord.Option(str, description="summoner's tag"),
    summoner_discord_id: discord.Option(str, description='Start typing from @ and chose your mate')):
    
    await message.respond(f'Is it your friend?')
    number_of_summoners = postgres_requests.PostgresRequest(message.guild.id).get_number_of_summoners()
    possible_summoners_for_server = postgres_requests. \
        PostgresRequest(message.guild.id). \
        get_possible_number_of_summoners()
    if number_of_summoners >= possible_summoners_for_server:
        await message.channel.send(f'You reached maximum number of summoners to add. {possible_summoners_for_server}')
        return

    channel_admins = postgres_requests.PostgresRequest(message.guild.id).get_admin_id_by_server()
    if str(message.author.id) in channel_admins:

        region = postgres_requests.PostgresRequest(message.guild.id).get_region_by_server_id()
        response_summoner = api_response.RiotApiResponse(region).get_puuid_by_nickname(summoner_name, summoner_tag)
        try:
            response_summoner['id']
        except KeyError:
            await message.channel.send('User with that nickname not found')
            return
        summoner_level = response_summoner['summonerLevel']
        summoner_icon = response_summoner['profileIconId']
        summoner_encrypt_id = response_summoner['id']
        version_number = api_response.RiotApiResponse().get_version_number()
        await message.channel.send(content=f'Nickname: {summoner_name}\n'
                                           f'Level: {summoner_level}\n'
                                           f'Profile icon\n')
        await message.channel.send(content=f'http://ddragon.leagueoflegends.com/cdn/{version_number}'
                                           f'/img/profileicon/{summoner_icon}.png')

        if summoner_discord_id[0:1] == '<@':
            discord_id = (summoner_discord_id.split('<')[-1])[1:-1]
        else:
            discord_id = None

        accept_view = AcceptanceView()
        await message.channel.send(view=accept_view)
        await accept_view.wait()
        if accept_view.value:
            summoner_addition_status_code = postgres_requests.PostgresRequest(message.guild.id). \
                insert_new_summoner(response_summoner['puuid'], summoner_name,
                                    discord_id, summoner_encrypt_id, summoner_level, message)
            if summoner_addition_status_code == 1:
                await message.channel.send('Summoner already added to database')
            else:
                await message.channel.send(f'Added {summoner_name} to database')

    else:
        await message.channel.send(only_admin_restriction)


@bot.command(description="Show leaderboard")
async def leaderboard(message):
    await message.respond('Leaderboard:')
    picture_generator.ImageGenerator('all_records', server_id=message.guild.id).draw_leaderboard('leaderboard')
    await message.channel.send(file=discord.File('images_to_send/leaderboard.png'))

@bot.command(description="Show all admins")
async def admins(message):
    admins_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_admin_id_by_server()
    position = 1
    string_of_admins = ''
    for admin in admins_list:
        string_of_admins += f'{position}. <@{admin}>\n'
        position += 1
    await message.respond(f"List of server\'s administrators\n{string_of_admins}")


@bot.command(description="Show all summoners")
async def summoners(message):
    string_of_summoners = generate_summoners_list(message)
    await message.respond(string_of_summoners)


@bot.command(description="Use it to add admin")
async def add_admin(message, admin_discord_id: discord.Option(str)):
    channel_admins = postgres_requests.PostgresRequest(message.guild.id).get_admin_id_by_server()
    if str(message.author.id) in channel_admins:
        discord_id = (admin_discord_id.split('<')[-1])[1:-1]
        admin_addition_status_code = postgres_requests.PostgresRequest(server_id=message.guild.id).\
            add_another_admin(channel_admins, discord_id)
        if admin_addition_status_code == 1:
            await message.respond("User already added to administrator's pool")
        else:
            await message.respond(f'Added {admin_discord_id} to admins list')
    else:
        await message.channel.send(only_admin_restriction)


@bot.command(description="Use it to delete admins")
async def delete_admin(message):
    admins_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_admin_id_by_server()
    if str(message.author.id) in admins_list:

        view = DeletionView(message, 'admin', admins_list)
        await message.respond("Chose admin to delete", view=view)
    else:
        await message.channel.send(only_admin_restriction)


@bot.command(description="Use it to delete summoners")
async def delete_summoner(message):
    admins_list = postgres_requests.PostgresRequest(server_id=message.guild.id).get_admin_id_by_server()
    if str(message.author.id) in admins_list:
        view = DeletionView(message, 'summoner')
        try:
            await message.respond("Chose summoner to delete", view=view)
        except discord.errors.ApplicationCommandInvokeError:
            await message.respond("Summoner\'s database is clear. No one to delete")

    else:
        await message.channel.send(only_admin_restriction)

##########CASINO###########
@bot.command(description="Show leaderboard of gamblers")
async def show_points_leaderboard(message):
    points_from_server = postgres_requests.PostgresRequest(server_id=message.guild.id).get_server_points()
    if len(points_from_server) == 0:
        text = f'There:man_with_veil: are no gamblers at your server:woman_mage: rn'
        title = 'Ooops:pinching_hand:'
        colour = discord.Color.red()
    else:
        gamblers = f'Top {embeds_decorator.get_random_emoji()}gamblers:{embeds_decorator.get_random_emoji()}\n'
        placement = 1
        for gambler in points_from_server:
            if placement == 1:
                medal = ':first_place:'
            elif placement == 2:
                medal = ':second_place:'
            elif placement == 3:
                medal = ':third_place:'
            else:
                medal = ''
            gamblers += f'{medal}{placement}. <@{gambler[0]}>: {str(gambler[1])}\n'
            placement += 1
        text = gamblers
        title = f'Leaderboard{embeds_decorator.get_random_emoji()}'
        colour = discord.Color.green()

    embed = discord.Embed(
    title=title,
    description=text,
    color=colour)
    random_gambler, random_gambler_path = embeds_decorator.get_gambler_photo()
    gambler_file = discord.File(random_gambler_path, filename=random_gambler)
    embed.set_thumbnail(url=f"attachment://{random_gambler}")
    await message.respond(file=gambler_file, embed=embed)


@bot.command(description="Show my point")
async def show_my_points(message):

    points_from_server = postgres_requests.PostgresRequest(server_id=message.guild.id).get_gamblers_points(message.author.id)
    if points_from_server[1] == 2:
        text = f"You've:dart: been added to the Gambler's List:game_die: and granted 1000:money_mouth::money_mouth: points"
    else:
        text = f"You :game_die:have {points_from_server[0]} points."
    embed = discord.Embed(title=':slot_machine:Points:moneybag:', description=text, color=discord.Color.yellow())
    
    random_gambler, random_gambler_path = embeds_decorator.get_gambler_photo()
    gambler_file = discord.File(random_gambler_path, filename=random_gambler)
    embed.set_thumbnail(url=f"attachment://{random_gambler}")
    await message.respond(file=gambler_file, embed=embed)


async def get_game_id(ctx: discord.AutocompleteContext):
    ctx.options["game_id"]
    active_bets = postgres_requests.PostgresRequest(ctx.interaction.guild.id).get_active_bets()

    return active_bets

def calculate_ratio(total_result):
    try:
        coef_on_win = total_result['total_points']/total_result['total_on_win']
    except ZeroDivisionError:
        coef_on_win = 0.0
    try:
        coef_on_lose = total_result['total_points']/total_result['total_on_lose']
    except ZeroDivisionError:
        coef_on_lose = 0.0     
    
    return [round(coef_on_win,2), round(coef_on_lose, 2)]

def gamblers_list(total_result):
    win = ''
    lose = ''
    for participant in total_result['participants']:
        if total_result['participants'][participant]['result_predicted'] == True:
            win += f"<@{participant}> - {total_result['participants'][participant]['bet_amount']} pts.   \n"
        else:
            lose += f"<@{participant}> - {total_result['participants'][participant]['bet_amount']} pts.\n"
    
    return [win[:-1], lose[:-1]]



@bot.command(description="Place a bet")
async def bet(
    message,
    result_text: discord.Option(str, name='result', description='Choose win or lose', choices=['Win', 'Lose'], required=True),
    amount: discord.Option(int, name='points', description='Enter the points amount', min_value=1, required=True),
    game_id: discord.Option(str, name='game_id', description='Game ID to bet on', autocomplete=get_game_id, required=True)):

    if result_text == 'Win':
        result = True
    else:
        result = False
    
    embed = discord.Embed(title='placeholder', description='placeholder', color=discord.Color.red())

    random_gambler, random_gambler_path = embeds_decorator.get_gambler_photo()
    gambler_file = discord.File(random_gambler_path, filename=random_gambler)
    embed.set_thumbnail(url=f"attachment://{random_gambler}")


    gamblers_points = postgres_requests.PostgresRequest(message.guild.id).get_gamblers_points(message.author.id)[0]
    current_amount, current_result = postgres_requests.PostgresRequest(message.guild.id).get_current_bet(message.author.id, game_id)

    if gamblers_points + current_amount < amount:
        text = f"Not enough :slot_machine:points!\nYou :man_in_motorized_wheelchair:only have {gamblers_points}:coin:"
        title = 'Ooops!:toilet: '

        embed.title = title
        embed.description = text
        embed.color = discord.Color.red()
        await message.respond(file=gambler_file, embed=embed)
        return

    if current_amount == 0: #If user is out of points or no bets made
        postgres_requests.PostgresRequest(message.guild.id).insert_bet(game_id, amount, message.author.id, result)
        postgres_requests.PostgresRequest(message.guild.id).update_points(message.author.id, gamblers_points, delta=0, points=amount)

        text = f"You placed {str(amount)}:moneybag: points on {result_text}.:money_with_wings:"
        title = "You've made a bet:money_mouth:"
    
    elif current_result != result: # If trying to change decision
        text = f"You can't change :sob:you decision after making a:money_with_wings: bet.\nOnly possible to increase:sob: amount."
        title = 'Ooops!:beers:'

        embed.title = title
        embed.description = text
        embed.color = discord.Color.red()
        await message.respond(file=gambler_file, embed=embed)
        return
    
    elif current_amount < amount and gamblers_points != 0: # If increasing the bet amount
        postgres_requests.PostgresRequest(message.guild.id).update_points(message.author.id, gamblers_points, delta=current_amount, points=amount)
        postgres_requests.PostgresRequest(message.guild.id).update_bet(amount, message.author.id, game_id)
        text = f"Raised:money_with_wings: bet points to {amount}:money_with_wings:"
        title = 'Success!'
    
    elif current_amount > amount: # If trying to decrease bet amount
        text = f"You can't:sob: descrease amount:toilet:  of points after:money_with_wings: bet was made"
        title = 'Ooops!:worried:'

        embed.title = title
        embed.description = text
        embed.color = discord.Color.red()
        await message.respond(file=gambler_file, embed=embed)
        return
    
    data_about_bets = postgres_requests.PostgresRequest(message.guild.id).get_bets_result(game_id)
    ratio = calculate_ratio(data_about_bets)
    gamblers_on_win, gamblers_on_lose = gamblers_list(data_about_bets)

    embed.title = title
    embed.description = text
    embed.color = discord.Color.green()
    embed.add_field(name=f"Win{embeds_decorator.get_random_emoji()}", value=f"{embeds_decorator.get_random_emoji()}coef. - **{ratio[0]}**\n{gamblers_on_win}")
    embed.add_field(name=f"Lose{embeds_decorator.get_random_emoji()}", value=f"{embeds_decorator.get_random_emoji()}coef. - **{ratio[1]}**\n{gamblers_on_lose}", inline=True)
    
    await message.respond(file=gambler_file, embed=embed)



###########################


# Service command for service chat to make some service staff
@bot.event
async def on_message(message):
    emoji = '👬'
    thumb_up = '👍'
    thumb_down = '👎'
    wow = '😯'
    if str(message.author) == "New game's stats#0000":
        await message.add_reaction(thumb_up)
        await message.add_reaction(thumb_down)

    if str(message.author) == "New Record Alert!!#0000":
        await message.add_reaction(wow)

    if message.author == bot.user and message.content == '???':
        await message.add_reaction(emoji)

    if message.channel.id == 1155118436070993960 and message.author.id == 394100136705392650:
        content = str(message.content)
        if content.startswith('$delete_server'):
            server_to_delete = content.split()[1]
        msg = f"DO you really want to delete {server_to_delete} server" 
        await message.channel.send(msg)

        accept_view = AcceptanceView()
        await message.channel.send(view=accept_view)
        await accept_view.wait()
        if accept_view.value:
            summoner_addition_status_code = postgres_requests.PostgresRequest(server_to_delete).delete_server_data()
            if summoner_addition_status_code == 1:
                await message.channel.send('Something went wrong. \n Check logs at vm by docker compose logs')
            else:
                await message.channel.send(f'Successfully deleted {server_to_delete} from db')


bot.run(token['discord_token'])

import time
import requests
import json
import yaml
import regions


class RiotApiResponse:

    def __init__(self, region='eu'):
        with open('TOKEN.yaml', 'r') as file_with_tokens:
            token = yaml.safe_load(file_with_tokens)
            self.header = {'X-Riot-Token': token['riot_token']}
        self.region = region
        self.route = regions.RegionDecider().get_route_by_region(self.region)

    def get_response_200(self, timeout, endpoint_last_game):
        """
        Recursive function to get response 200
        :param timeout: time to wait before another request
        :param endpoint_last_game: endpoint to send request to
        :return: response
        """
        try:
            request_jsoned = requests.get(endpoint_last_game,
                                          headers=self.header,
                                          timeout=40)  # getting response
            MetricsApiResponse().increment_metrics()
        except Exception as error:
            print(f"\033[91m[ERROR] \033[0m We've encountered a connection issue. ROOT_COUSE_URL: {endpoint_last_game}")
            pass
            request_jsoned = requests.get('https://192.168.0.22/fake_addr', timeout=0.1)
        time.sleep(timeout)
        return request_jsoned

    def get_list_of_last_games(self, puuids):
        """

        :param puuids: List of unique IDs of my crewmates
        :return: list of all last games my friends played
        """
        list_of_last_games = []
        for puuid in puuids:
            endpoint = f'https://{self.route}.api.riotgames.com/' \
                       f'lol/match/v5/matches/by-puuid/{puuid}/ids?start=0&count=1'
            timeout = 1
            request_jsoned = self.get_response_200(timeout, endpoint)
            while not request_jsoned.ok:
                request_jsoned = self.get_response_200(timeout, endpoint)
                timeout += 1
            try:
                request_plain = json.loads(request_jsoned.text)[0]
            except IndexError:
                request_plain = None

            time.sleep(1)
            list_of_last_games.append(request_plain)

        return list_of_last_games

    def get_unique_coop_games(self, puuids):
        """

        :return: UNIQUES entries in all the games my friends played
        """
        unique_coop_games = []
        games = self.get_list_of_last_games(puuids)
        for game in games:
            if game and games.count(game) > 1 and game not in unique_coop_games:
                unique_coop_games.append(game)

        return unique_coop_games

    def get_game_data(self, game_id):
        """

        :param game_id: Game id to make review
        :return: jsoned review on game
        """
        endpoint = f'https://{self.route}.api.riotgames.com/lol/match/v5/matches/{game_id}'
        timeout = 1
        request_jsoned = self.get_response_200(timeout, endpoint)
        while not request_jsoned.ok:
            request_jsoned = self.get_response_200(timeout, endpoint)
            timeout += 1
        request_plain = json.loads(request_jsoned.text)

        return request_plain

    def get_puuid_by_nickname(self, nickname, tag):
        endpoint = f'https://{self.route}.api.riotgames.com/riot/account/v1/accounts/by-riot-id/{nickname}/{tag}'
        timeout = 1
        response = self.get_response_200(timeout, endpoint)
        while response.status_code not in (200, 400, 404):
            response = self.get_response_200(timeout, endpoint)
            timeout += 1

        jsoned_response = json.loads(response.text)        
        if response.status_code == 404:
            return jsoned_response

        self.machine_region = regions.RegionDecider().generate_correct_machine_region(self.region)
        endpoint = f"https://{self.machine_region}.api.riotgames.com/lol/summoner/v4/summoners/by-puuid/{jsoned_response['puuid']}"
        timeout = 1
        response = self.get_response_200(timeout, endpoint)
        while response.status_code not in (200, 400, 404):
            response = self.get_response_200(timeout, endpoint)
            timeout += 1
        jsoned_response = json.loads(response.text)

        return jsoned_response

    def get_version_number(self):
        endpoint = 'https://ddragon.leagueoflegends.com/api/versions.json'
        timeout = 1
        response = self.get_response_200(timeout, endpoint)
        while not response.ok:
            response = self.get_response_200(timeout, endpoint)
            timeout += 1
        jsoned_response = (json.loads(response.text))[0]

        return jsoned_response
    
    def get_champions_data(self, champion_id):
        version = self.get_version_number()
        endpoint = f'https://ddragon.leagueoflegends.com/cdn/{version}/data/en_US/champion.json'
        timeout = 1
        response = self.get_response_200(timeout, endpoint)
        while not response.ok:
            response = self.get_response_200(timeout, endpoint)
            timeout += 1
        jsoned_response = (json.loads(response.text))

        for champion, champion_data in jsoned_response['data'].items():
            if int(champion_data['key']) == int(champion_id):
                return {'name': champion,
                        'id': int(champion_id)}
    
    def get_game_started_for_gambling(self, participants):
        self.machine_region = regions.RegionDecider().generate_correct_machine_region(self.region)
        list_of_current_games = []
        list_of_gamble_games = []
        game_data = {}
        for participant in participants:
            timeout = 1
            try:
                jsoned_response = ''
                endpoint = f'https://{self.machine_region}.api.riotgames.com/lol/spectator/v4/active-games/by-summoner/{participant}'
                response = self.get_response_200(timeout, endpoint)
                while response.status_code not in (200, 400, 404):
                    response = self.get_response_200(timeout, endpoint)
                    timeout += 1
                jsoned_response = json.loads(response.text)
                try:
                    game_id = jsoned_response['gameId']
                    if jsoned_response['gameType'] != 'MATCHED_GAME':
                        game_id = None

                except IndexError:
                    game_id = None

                time.sleep(1)
                full_game_id = f'{self.machine_region}_{game_id}'
                game_data[full_game_id] = jsoned_response
                if game_id:
                    list_of_current_games.append(full_game_id)

            except:
                continue
        for game in list_of_current_games:
            if game and list_of_current_games.count(game) > 1 and game not in list_of_gamble_games:
                list_of_gamble_games.append(game)
           
        return list_of_gamble_games, game_data

    



class MetricsApiResponse:
    def __init__(self):
        self.endpoint = f'http://metrics_exporter:5000'

    def increment_metrics(self):
        target = '/increment_call'
        requests.get(f'{self.endpoint}{target}')

    def receive_metrics(self):
        target = '/receive_data'
        request = requests.get(f'{self.endpoint}{target}')
        jsoned_response = json.loads(request.text)

        return jsoned_response

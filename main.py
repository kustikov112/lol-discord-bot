import copy
import time
import psycopg2
import picture_generator
import postgres_requests
from postgres_requests import PostgresRequest
import records_registration
import api_response
import webhook_sender
import ai_connector

# Sleep for testing purposes to have enough time to truncate table
time.sleep(5)

# TODO: add docstrings


class GamesFromServer:
    def __init__(self, server_id):
        self.server_id = server_id
        # Since DB starts slower when script, there is exception to wait a bit, before DB is ready to catch requests
        try:
            self.initiate_records = records_registration.get_top_stats(self.server_id)
        except psycopg2.OperationalError:
            time.sleep(20)
            self.initiate_records = records_registration.get_top_stats(self.server_id)
        self.old_records = copy.deepcopy(self.initiate_records)
        self.crew = PostgresRequest(self.server_id).get_list_of_crewmates()
        self.region = postgres_requests.PostgresRequest(self.server_id).get_region_by_server_id()

    def send_records(self, dict_of_records):
        """

        :param dict_of_records:dict with new records found from last game
        :return: sends message to discord via webhook
        """
        for key, record in dict_of_records.items():
            text_filler, puuid, value, champion_name = (
                self.old_records[key]["record"],
                record[0],
                record[1],
                record[2]
            )
            picture_generator.ImageGenerator(image_trigger='new_record',
                                             server_id=self.server_id,
                                             champion_name=champion_name
                                             ).generate_new_record_pic(puuid, text_filler, value)
            webhook_sender.SendWebhookMessage(self.server_id).send_record_notification(puuid)

    def send_result(self):
        """

        :param start_records: Getting previous records to compare with new one and decide if we have new Records
        :return: "previous records"
        """
        viewed_games = PostgresRequest(self.server_id).get_list_of_viewed_games()
        recent_games = api_response.RiotApiResponse(region=self.region).get_unique_coop_games(self.crew)

        for game in recent_games:
            if game not in viewed_games:
                stat_of_the_game = api_response.RiotApiResponse(region=self.region).get_game_data(game)
                game_result = PostgresRequest(self.server_id).adding_data_to_psql(stat_of_the_game, self.crew)
                match_id = stat_of_the_game['metadata']['matchId']
                bets = PostgresRequest(self.server_id).get_bets_result(match_id)

                if game_result == 'remake':
                    print(f'\033[93m[INFO] \033[0mOverviewed game with ID {match_id} for server:{self.server_id}. Result is - Remake')
                    if bets['total_bets'] != 0:
                        webhook_sender.SendWebhookMessage(self.server_id, casino=True).revert_cash_on_remake(bets)
                    time.sleep(0.5)
                    return

                new_records_from_psql = records_registration.get_top_stats(self.server_id)
                picture_generator.ImageGenerator(image_trigger='result',
                                                 server_id=self.server_id,
                                                 data_from_response=stat_of_the_game,
                                                 game_result=game_result).generate_result_picture()
                time.sleep(1)
                webhook_sender.SendWebhookMessage(self.server_id).send_game_stats(match_id)
                print(f'\033[93m[INFO] \033[0mOverviewed game with ID {match_id} for server:{self.server_id}')
                # # GENAI staff ##################################################################################
                server_localization = PostgresRequest(self.server_id).get_region_by_server_id()
                # print(f'\033[93m[DEBUG] \033[0m running GenAI functions')
                gen_ai_responce = ai_connector.GenAi(crew=self.crew, data=str(stat_of_the_game), localization=server_localization).generate_summary_by_lambda_function()
                # print(f'\033[93m[DEBUG] \033[0m Sending webhook')
                webhook_sender.SendWebhookMessage(self.server_id).send_genai_conclusion(gen_ai_responce)
                # ################################################################################################
                if new_records_from_psql != self.old_records:
                    dict_of_records = records_registration.compare_stats_after_game(
                        self.old_records, new_records_from_psql
                    )
                    self.send_records(dict_of_records)
                
                if bets['total_bets'] != 0:
                    webhook_sender.SendWebhookMessage(self.server_id, casino=True).send_gamble_result(bets, game_result)

        time.sleep(1)



def gambling_notification(games_to_check, server, crewmates, game_data):
    gambled_games = PostgresRequest(server).get_list_of_gambled_games()
    for game in games_to_check:
        if game not in gambled_games:
            webhook_sender.SendWebhookMessage(server, casino=True).send_gamble_notification(crewmates, game, game_data)
            PostgresRequest(server).insert_initial_bet(game)



def number_of_requests():
    response = api_response.MetricsApiResponse().receive_metrics()
    requests_per_second = response['amount_of_calls_per_second']
    requests_per_2_minutes = response['amount_of_calls_per_2_minutes']
    last_time_updated = response['last_time_updated']

    print(f'\033[93m[INFO] \033[0mRequests per second: {requests_per_second}')
    print(f'\033[93m[INFO] \033[0mRequests per 2 minutes: {requests_per_2_minutes}')

    return last_time_updated


def main():
    previous_time_checked_requests = time.time()
    while True:
        all_servers = postgres_requests.PostgresRequest().get_list_of_servers()
        servers_to_gamble = postgres_requests.PostgresRequest().get_list_of_gambling_servers()
        if time.time()-previous_time_checked_requests > 43200:
            previous_time_checked_requests = number_of_requests()

        # for server in servers_to_gamble:
        #     crewmates = PostgresRequest(server).get_list_of_encrypted_ids()
        #     region = postgres_requests.PostgresRequest(server).get_region_by_server_id()
        #     games_to_check, game_data = api_response.RiotApiResponse(region).get_game_started_for_gambling(crewmates)
        #     gambling_notification(games_to_check, server, crewmates, game_data)

        for server in all_servers:
            GamesFromServer(server).send_result()
        time.sleep(0.1)


main()


# TODO: Make emojis randomly
# TODO: update bot picture on the fly
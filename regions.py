class RegionDecider:
    def __init__(self):

        self.routings = {'americas': ['NA', 'BR', 'LAN', 'LAS'],
                   'asia': ['KR', 'JP'],
                   'europe': ['EUNE', 'EUW', 'TR', 'RU'],
                   'SEA': ['OCE', 'PH2', 'SG2', 'TH2', 'TW2', 'VN2']
                    }
        self.machine_routing = {
            'NA': 'NA1',
            'BR': 'BR1',
            'LAN': 'LA1',
            'LAS': 'LA2',
            'KR': 'KR',
            'JP': 'JP1',
            'EUNE': 'EUN1',
            'EUW': 'EUW1',
            'TR': 'TR1',
            'RU': 'RU',
            'OCE': 'OC1',
            'PH2': 'PH2',
            'SG2': 'SG2',
            'TH2': 'TH2',
            'TW2': 'TW2',
            'VN2': 'VN2'
        }

    def generate_string_of_regions(self):
        string_of_regions = ''
        for list_of_regions in self.routings.values():
            for region in list_of_regions:
                string_of_regions += f'{region}\n'

        return string_of_regions

    def get_route_by_region(self, region):
        route = ''
        for key, list_of_regions in self.routings.items():
            if region.upper() in list_of_regions:
                route = key
                break
        return route

    def generate_correct_machine_region(self, region):
        return self.machine_routing[region.upper()]

    def generate_region_list(self):
        complete_list_of_regions = []
        for list_of_regions in self.routings.values():
            for region in list_of_regions:
                complete_list_of_regions.append(region)

        return complete_list_of_regions

from flask import Flask
import time
import json
import logging


class Collector:
    def __init__(self):
        self.calls_collected = 0
        self.nullified_time = time.time()

    def increment_call(self):
        self.calls_collected += 1

        return self.calls_collected

    def nullify_time(self):
        self.calls_collected = 0
        previous_time = self.nullified_time
        self.nullified_time = time.time()

        return previous_time


app = Flask(__name__)

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

amount_of_calls = Collector()


@app.route('/increment_call')
def call_api_incrementer():
    amount_of_calls.increment_call()
    response = {'amount_of_calls': amount_of_calls.calls_collected,
                'time': amount_of_calls.nullified_time}
    jsoned_response = json.dumps(response, indent=4)

    return jsoned_response


@app.route('/receive_data')
def return_data():
    time_between_responses = time.time() - amount_of_calls.nullified_time
    calls = amount_of_calls.calls_collected
    calls_per_second = calls/time_between_responses
    calls_per_2_minutes = calls_per_second*120
    last_time = amount_of_calls.nullify_time()
    response = {'amount_of_calls_per_second': calls_per_second,
                'amount_of_calls_per_2_minutes': calls_per_2_minutes,
                'last_time_updated': last_time
                }
    jsoned_response = json.dumps(response, indent=4)

    return jsoned_response


app.run(host='0.0.0.0')

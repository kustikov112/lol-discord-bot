from PIL import Image, ImageFont, ImageDraw
import requests
import os
import time
import datetime
import postgres_requests
import textwrap
import api_response
import yaml
import json

import records_registration

spells = {
    21: 'Barrier',
    1: 'Cleanse',
    14: 'Ignite',
    3: 'Exhaust',
    4: 'Flash',
    6: 'Ghost',
    7: 'Heal',
    13: 'Clarity',
    11: 'Smite',
    39: 'Mark',
    32: 'Mark',
    12: 'Teleport',
}


def renew_picture(purpose_of_picture, amount_of_stats=0, amount_of_records=0):
    """

    :param purpose_of_picture: cause of new picture request. May be "result", "one_record", "all_records"
    :param amount_of_stats: amount of stats to decide the width of RESULT.png
    :param amount_of_records: amount of stats to decide the height of leaderboard.png
    :return: saves new picture
    """
    with open('TOKEN.yaml', 'r') as f:
        token = yaml.safe_load(f)['unsplash_api_key']
        
    picture_theme = 'forest'
    size = 'w=560&h=562'
    if purpose_of_picture == 'result':
        width = str(1120+amount_of_stats*140)
        size = f'w={width}&h=1124'
    elif purpose_of_picture == 'one_record':
        size = 'w=280&h=170'
    elif purpose_of_picture == 'all_records':
        height = str(155+(int(amount_of_records//3))*155)
        size = f'w=810&h={height}'
    url_for_pic = f'https://api.unsplash.com/photos/random/?orientation=landscape&query={picture_theme}&client_id={token}'
    # get random pic
    response = requests.get(url_for_pic).content
    # get raw pic URL
    picture_url = f"{json.loads(response)['urls']['raw']}&{size}&q=20&fit=scale"
    picture_in_response = requests.get(picture_url).content

    # remove picture to handle errors if occurred while opening file
    try:
        os.remove(f'images_empty/{purpose_of_picture}.png')
    except FileNotFoundError:
        pass
    try:
        with open(f'images_empty/{purpose_of_picture}.png', 'wb+') as picture_template:
            picture_template.write(picture_in_response)
    except FileExistsError:
        pass


def update_pictures(item, item_name):
    """

    :param item: what is the type of missed picture. it now may be champion or item
    :param item_name: name of the item. digit code of item or champion name
    :return: saves new picture
    """
    #   save loading screen pic
    if item == 'champion':
        with open(f'img/loading/{item_name}_0.png', 'bw+') as picture:
            picture.write(requests.get(
                f'http://ddragon.leagueoflegends.com/cdn/img/champion/loading/{item_name}_0.jpg').content
                          )
            time.sleep(1)
    #   get version number to receive square icon of champ
    version_number = api_response.RiotApiResponse().get_version_number()
    #   square picture of champ
    with open(f'img/{item}/{item_name}.png', 'bw+') as picture:
        picture.write(requests.get(
            f'http://ddragon.leagueoflegends.com/cdn/{version_number}/img/{item}/{item_name}.png').content
                         )


class ImageGenerator:

    def __init__(self, image_trigger, server_id=None, data_from_response=None, game_result=True, champion_name=None):
        self.font_basic = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 28)
        self.font_KDA = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 30)
        self.font_constant_text = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 36)
        self.font_small = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 20)

        self.font_new_record_title = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 30)
        self.font_new_record = ImageFont.truetype('font/DejaVuSans-Bold.ttf', 25)

        self.font_leaderboard_top = ImageFont.truetype('font/marvinFont.ttf', 13)
        self.font_leaderboard_regular = ImageFont.truetype('font/marvinFont.ttf', 13)
        self.font_leaderboard_title = ImageFont.truetype('font/marvinFont.ttf', 13)

        # self.color_for_static_text = (223, 210, 30)
        # self.color_for_iterable_text = (223, 210, 30)
        self.color_for_static_text = (200, 200, 20)
        self.color_for_iterable_text = (200, 200, 200)
        self.color_for_stat_names = (210, 210, 0)
        self.color_for_kda_sum = (225, 128, 0)

        self.new_record_color = (223, 210, 30)

        self.top_five_leader_color = (223, 210, 30)
        self.top_five_color = (223, 210, 30)
        self.top_five_title_color = (223, 210, 30)

        self.number_of_players_in_team = 5
        try:
            self.match_id = data_from_response['metadata']['matchId']
            self.duration = data_from_response['info']['gameDuration']
            self.data_from_response = data_from_response
        except TypeError:
            pass
        self.game_result = game_result
        self.amount_of_stats = 0
        self.amount_of_records = 0
        self.list_of_records = []
        self.server_id = server_id
        stats_names_and_fillers = records_registration.get_top_stats(self.server_id)
        for stat in stats_names_and_fillers:
            if stats_names_and_fillers[stat]['stats_after_game']:
                self.amount_of_stats += 1
            if stats_names_and_fillers[stat]['record']:
                self.amount_of_records += 1
                self.list_of_records.append(stat)
        self.background = self.open_file(image_trigger, champion_name)
        self.stats_with_fillers = stats_names_and_fillers
        self.drawable_background = ImageDraw.Draw(self.background)


    def open_file(self, image_trigger, champion_name=None):
        """
        opening file to start editing
        :param image_trigger: cause of image creation
        :param champion_name: champion name to open picture in case of record
        :return: editable image as self.background
        """
        if image_trigger == 'new_record':
            my_image = Image.open(f'img/loading/{champion_name}_0.jpg')

        else:
            renew_picture(image_trigger, self.amount_of_stats, self.amount_of_records)
            my_image = Image.open(f'images_empty/{image_trigger}.png')

        return my_image

    def insert_images(self):
        """
        one big function with ssub functions to insert images to Result image
        :return:
        """
        participants = self.data_from_response['info']['participants']

        def insert_items():
            """
            inserting items
            :return:
            """
            item_position = [500, 176, 540, 216]  # Position of first item (X-left, y-upper, X-right, y-down)
            size_items = (40, 40)  # size of Item picture
            number_of_items = 6
            for i, participant in enumerate(participants):
                item_position[0], item_position[2] = 500, 540

                if i == self.number_of_players_in_team:
                    item_position[1] += 40
                    item_position[3] += 40

                for item_positional_number in range(number_of_items):
                    item_name = participant[f'item{item_positional_number}']
                    if item_name != 0:
                        try:
                            item_to_insert = Image.open(f'img/item/{item_name}.png')
                        except FileNotFoundError:
                            update_pictures('item', item_name)
                            item_to_insert = Image.open(f'img/item/{item_name}.png')
                        item_resized = item_to_insert.resize(size_items)
                        self.background.paste(item_resized, item_position)
                        item_position[0] += 40
                        item_position[2] += 40
                item_position[1] += 80
                item_position[3] += 80

        def insert_characters():

            champion_position = [158, 158, 238, 238]  # Position of 1st champ picture
            size_champ_pics = (80, 80)  # size of champ picture
            for i, participant in enumerate(participants):

                if i == self.number_of_players_in_team:
                    champion_position[1] += 40
                    champion_position[3] += 40

                character = participant['championName']
                try:
                    champion_square = Image.open(f'img/champion/{character}.png')
                except FileNotFoundError:
                    update_pictures('champion', character)
                    champion_square = Image.open(f'img/champion/{character}.png')
                champion_square_resized = champion_square.resize(size_champ_pics)
                self.background.paste(champion_square_resized, champion_position)
                champion_position[1] += 80
                champion_position[3] += 80

        def insert_spells():
            spell_position = [118, 158, 158, 198]
            size_spells = (40, 40)
            amount_of_spells = 2
            for i, participant in enumerate(participants):

                if i == self.number_of_players_in_team:
                    spell_position[1] += 40
                    spell_position[3] += 40

                for spell_number in range(amount_of_spells):
                    spell_id = participant[f'summoner{spell_number+1}Id']
                    spell_name = spells[spell_id]
                    spell_picture_name = 'Summoner' + spell_name
                    spell_picture = Image.open(f'img/spell/{spell_picture_name}.png')
                    spell_picture_resized = spell_picture.resize(size_spells)
                    self.background.paste(spell_picture_resized, spell_position)
                    spell_position[1] += 40
                    spell_position[3] += 40

        insert_items()
        insert_spells()
        insert_characters()

    def insert_static_text(self):
        """
        insert not iterable texts in result picture
        :return:
        """
        background_width, background_height = self.background.size
        game_duration = self.duration

        def insert_match_duration():

            match_duration_position = (60, 48)
            duration_string = str(datetime.timedelta(seconds=game_duration))  # Getting the duration of game

            self.drawable_background.multiline_text(
                match_duration_position,
                f'Match duration\n{duration_string}',
                self.color_for_static_text,
                align='center',
                font=self.font_constant_text
            )

        def insert_result():
            if self.game_result:
                result = 'Victory'
            else:
                result = 'Defeat'
            result_position = ((int(background_width / 2)), 48)

            self.drawable_background.text(
                result_position,
                result,
                self.color_for_static_text,
                anchor='mm',
                font=self.font_constant_text
            )

        def insert_metadata():

            metadata_center_position = [background_width-300, 1040]
            match_id_position = (100, 1100)
            scores = postgres_requests.PostgresRequest(self.server_id).get_score()
            team_name = 'Buoys'
            names = (team_name, 'vs.', 'the World')
            alignment = ('rm', 'mm', 'lm')
            score = f'{scores["crewmates"]} : {scores["world"]}'
            vs_length = self.font_constant_text.getlength('vs.')
            position_for_team_names = metadata_center_position.copy()
            position_for_team_names[0] -= vs_length
            names_and_alignments = zip(names, alignment)
            for name, alignment in names_and_alignments:

                self.drawable_background.text(
                    position_for_team_names,
                    name,
                    self.color_for_static_text,
                    anchor=alignment,
                    font=self.font_constant_text
                )
                position_for_team_names[0] += vs_length
            metadata_center_position[1] += 40
            self.drawable_background.text(
                metadata_center_position,
                score,
                self.color_for_static_text,
                anchor='mm',
                font=self.font_constant_text
            )
            self.drawable_background.text(
                match_id_position,
                self.match_id,
                self.color_for_static_text,
                anchor='mm',
                font=self.font_small
            )

        insert_match_duration()
        insert_result()
        insert_metadata()

    def insert_iterable_text(self):
        """
        inserting iterable text
        :return:
        """
        participants = self.data_from_response['info']['participants']

        def insert_permanent_stats():
            def insert_titles():
                titles_position = [(840, 140), (970, 140)]
                permanent_titles = ['Position', 'K/D/A']
                text_to_insert = zip(titles_position, permanent_titles)
                for text in text_to_insert:
                    self.drawable_background.text(
                        text[0],
                        text[1],
                        self.color_for_stat_names,
                        anchor='ma',
                        font=self.font_small
                    )

            def insert_nicknames():
                nickname_position = [240, 180]
                for i, participant in enumerate(participants):
                    if i == self.number_of_players_in_team:
                        nickname_position[1] += 40
                    self.drawable_background.text(
                        nickname_position,
                        participant['summonerName'],
                        self.color_for_iterable_text,
                        anchor='la',
                        font=self.font_basic
                    )
                    nickname_position[1] += 80

            def insert_position():
                position_position = [840, 186]
                for i, participant in enumerate(participants):
                    position = participant['individualPosition']
                    if position == 'Invalid':
                        position = 'ARAM'

                    if i == self.number_of_players_in_team:
                        position_position[1] += 40

                    self.drawable_background.text(
                        position_position,
                        position,
                        self.color_for_iterable_text,
                        anchor='ma',
                        font=self.font_basic
                    )
                    position_position[1] += 80

            def insert_kda():
                position_kda = [970, 186]
                team_kda = {'kills': 0,
                            'deaths': 0,
                            'assists': 0}
                for i, participant in enumerate(participants):
                    kills = participant['kills']
                    deaths = participant['deaths']
                    assists = participant['assists']
                    text_kda = f'{kills}/{deaths}/{assists}'

                    team_kda['kills'] += kills
                    team_kda['deaths'] += deaths
                    team_kda['assists'] += assists

                    self.drawable_background.text(
                        position_kda,
                        text_kda,
                        self.color_for_iterable_text,
                        anchor='ma',
                        font=self.font_basic
                    )
                    position_kda[1] += 80

                    if i+1 == self.number_of_players_in_team or i+1 == self.number_of_players_in_team*2:
                        position_kda[1] -= 40
                        self.drawable_background.text(
                            position_kda,
                            f"{team_kda['kills']}/{team_kda['deaths']}/{team_kda['assists']}",
                            self.color_for_kda_sum,
                            anchor='ma',
                            font=self.font_KDA
                        )
                        position_kda[1] += 80
                        team_kda = {'kills': 0,
                                    'deaths': 0,
                                    'assists': 0}

            insert_titles()
            insert_nicknames()
            insert_position()
            insert_kda()

        def insert_changeable_stats():

            gap_between_stats = 140
            last_coordinates = [970, 140]
            for stat in self.stats_with_fillers:
                titles_for_stats = self.stats_with_fillers[stat]['stats_after_game']
                if titles_for_stats:
                    last_coordinates[1] = 140
                    last_coordinates[0] += gap_between_stats
                    titles_for_stats_in_list = textwrap.wrap(titles_for_stats, width=12)
                    for word in titles_for_stats_in_list[::-1]:
                        self.drawable_background.multiline_text(
                            last_coordinates,
                            word,
                            self.color_for_stat_names,
                            anchor='ma',
                            font=self.font_small
                        )
                        last_coordinates[1] -= 20

                    last_coordinates[1] = 186
                    for i, participant in enumerate(participants):
                        if i == self.number_of_players_in_team:
                            last_coordinates[1] += 40

                        if stat == 'totalMinionsKilled':
                            value = participant['neutralMinionsKilled'] + participant[stat]
                        else:
                            value = participant[stat]
                        self.drawable_background.text(
                            last_coordinates,
                            str(value),
                            self.color_for_iterable_text,
                            anchor='ma',
                            font=self.font_basic
                        )
                        last_coordinates[1] += 80

        insert_changeable_stats()
        insert_permanent_stats()

    def generate_result_picture(self):
        self.background.putalpha(40)
        self.insert_static_text()
        self.insert_iterable_text()
        self.insert_images()
        self.background.save('images_to_send/result.png', quality=100)

    def generate_new_record_pic(self, puuid, filler, value):
        """
        generate New Record picture
        :param puuid: puuid of player who hit the record
        :param filler: text fillers to put into new record picture
        :param value: value of new record
        :return:
        """
        self.background.putalpha(80)
        nickname = postgres_requests.PostgresRequest(self.server_id).get_name_by_puuid(puuid)

        self.drawable_background.text((10, 25), 'New record!!',
                                      self.new_record_color, font=self.font_new_record_title)
        self.drawable_background.multiline_text((10, 420), f'{nickname} {filler[0]}\n'
                                                           f'{str(value)} {filler[1]}\nin the last game',
                                                           self.new_record_color, font=self.font_new_record)

        self.background.save('images_to_send/new_record.png', quality=100)

    def insert_one_stat(self, stat_name, coordinates=[18, 28]):
        """
        insert top 5 values from db for concrete stat
        :param stat_name: name of the stat
        :param coordinates: coordinates to start putting data
        :return:
        """
        top_data = records_registration.get_top_five_overall(stat_name, self.server_id)
        pretty_stat_name = self.stats_with_fillers[stat_name]['leaderboard']
        coordinates = coordinates.copy()
        self.drawable_background.text(coordinates,
                                      pretty_stat_name,
                                      self.top_five_title_color,
                                      font=self.font_leaderboard_title)
        coordinates[1] += 22

        for i, record in enumerate(top_data):
            # highlight leader
            if i == 0:
                color = self.top_five_leader_color
                font = self.font_leaderboard_top
            else:
                color = self.top_five_color
                font = self.font_leaderboard_regular

            value = top_data[i][4]
            nickname = top_data[i][2]
            position = i+1
            self.drawable_background.text(coordinates, f'{position}. {nickname}', color, font=font)
            coordinates[0] += 190
            self.drawable_background.text(coordinates, str(value), color, font=font)
            coordinates[0] -= 190
            coordinates[1] += 20

    def draw_leaderboard(self, stat_name):
        """
        in case full leaderboard requested we are iterating through all stats and creating big picture with all
        top5 posible.
        :param stat_name:
        :return:
        """
        self.background.putalpha(40)
        if stat_name != 'leaderboard':
            self.insert_one_stat(stat_name)

        else:
            coordinates = [18, 28]
            for i, stat in enumerate(self.list_of_records):
                self.insert_one_stat(stat, coordinates)
                coordinates[0] += 270
                if (i+1) % 3 == 0:
                    coordinates[0] = 18
                    coordinates[1] += 145

        self.background.save('images_to_send/leaderboard.png', quality=100)

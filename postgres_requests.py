import datetime
import psycopg2
import yaml

import regions
import service_webhook_sender

class PostgresRequest:
    def __init__(self, server_id=None):
        database_name = "lol_db"
        username = "lol_db"
        host = 'localhost'
        password = "example"
        with open("stats.yaml", "r") as file:
            self.stats_names_and_fillers = yaml.safe_load(file)
        try:
            self.conn = psycopg2.connect(f"dbname={database_name} user={username} host = {host} password = {password}")
        except psycopg2.OperationalError:
            host = 'db'
            self.conn = psycopg2.connect(f"dbname={database_name} user={username} host = {host} password = {password}")
        self.cur = self.conn.cursor()
        self.server_id = server_id

    def get_name_by_puuid(self, puuid):
        self.cur.execute(f"select nickname from crew where puuid = '{puuid}'")
        name = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        return name[0]
    
    def get_name_by_encrypted_id(self, encrypted_id):
        self.cur.execute(f"select nickname from crew where encrypt_id = '{encrypted_id}'")
        name = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        return name[0]

    def get_discord_id_by_puuid(self, puuid):
        self.cur.execute(f"select discord_id from crew where puuid = '{puuid}'")
        discord_id = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        return discord_id[0]

    def generate_query_to_post_to_psql(self, data, row_number):
        """

        :param data: Data for that character from the recent game
        :param row_number: row number calculated from parent function
        :return: text of query and tuple of stats to add to psql table
        """
        parameters = "playid"  # Initiate not blank string to add stats with loop
        s_fillers = "%s"
        stat_values = [row_number]
        for stat in self.stats_names_and_fillers.keys():
            parameters += f", {stat}"
            s_fillers += ", %s"
            value = data[stat]
            if stat == "totalMinionsKilled":
                value = int(data["totalMinionsKilled"]) + int(data["neutralMinionsKilled"])
            if stat == "longestTimeSpentLiving" and value == "0":
                value = data['timePlayed']
            stat_values.append(value)
        text = f"insert into play ({parameters}) values ({s_fillers})"

        return text, tuple(stat_values)

    def adding_data_to_psql(self, game_data, crewmates):
        """

        :param game_data: jsoned data from concrete game
        :param crewmates: list of all my known crewmates
        :return: nothing
        """

        dt = datetime.datetime.now()
        game_time = dt.strftime("%Y-%m-%d %H:%M:%S")
        match_id = game_data["metadata"]["matchId"]
        duration = game_data["info"]["gameDuration"]
        participants_this_game = game_data["info"]["participants"]
        match_mode = game_data["info"]["gameMode"]
        game_mode = game_data["info"]["gameType"]
        plays_number = []
        state = ""

        for player_stat in participants_this_game:
            for mate in crewmates:
                if mate == player_stat["puuid"]:
                    self.cur.execute("select playid from play order by playid desc limit 1;")
                    try:
                        row = int(self.cur.fetchone()[0]) + 1
                    except:
                        row = 1
                    query = self.generate_query_to_post_to_psql(player_stat, row)
                    text_of_query = query[0]
                    tuple_for_query = query[1]
                    self.cur.execute(text_of_query, tuple_for_query)

                    state = player_stat["win"]
                    remake = player_stat['gameEndedInEarlySurrender']
                    plays_number.append(row)
        while len(plays_number) < 5:
            plays_number.append(0)
        if remake == 'true':
            state = 'remake'
        self.cur.execute(
            "insert into game (datetime, gameid, state, duration, player1, "
            "player2, player3, player4, player5, match_mode, game_mode, server_id)"
            " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (
                game_time,
                match_id,
                state,
                duration,
                plays_number[0],
                plays_number[1],
                plays_number[2],
                plays_number[3],
                plays_number[4],
                match_mode,
                game_mode,
                self.server_id
            ),
        )
        self.update_activity()
        return state

    def get_list_of_viewed_games(self):
        """

        :return: List of all the viewed games from PSQL
        """

        list_of_viewed_games = []

        self.cur.execute(f"select gameid from game where server_id = {self.server_id}")
        games = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for game in games:
            list_of_viewed_games.append(game[0])

        return list_of_viewed_games

    def get_list_of_crewmates(self):
        """

        :return: List of all crewmates
        """

        list_of_crewmates = []

        self.cur.execute(f"select puuid from crew where server_id = {self.server_id}")
        members = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for member in members:
            list_of_crewmates.append(member[0])

        return list_of_crewmates

    def get_stat_by_plays(self, stat_for_monitoring, per_minute, limit):
        """

        :param stat_for_monitoring: name of the stat to get value
        :param per_minute: if stat is "per_minute" we have to divide it on match duration
        :param limit: limit records to get from db. 1 for top record. 5 for top5
        :return: tuple of top stat by requested in function
        """

        game_types = "and game.game_mode = 'MATCHED_GAME' and game.match_mode = 'CLASSIC'"
        #game_types = ""

        if per_minute:
            stat = f"round({stat_for_monitoring}/(duration/60)::numeric ,2)::float as stat"
        else:
            stat = f"{stat_for_monitoring} as stat"
        query_text = f"select puuid, championname, summonerName, game.duration, {stat} from play" \
                     f" inner join game on play.playid" \
                     f" in (game.player1,game.player2,game.player3,game.player4,game.player5)" \
                     f"where puuid in (select puuid from crew where server_id = {self.server_id}) " \
                     f"{game_types} and game.duration > 240 order by stat desc limit {limit};"

        self.cur.execute(query_text)
        parameters = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return parameters

    def get_score(self):
        """
        :return: dict with "Won" and "Lost" games
        """
        total_score = {}
        win_query = f"select count(*) from game where state = 'true' and server_id = {self.server_id}"
        lose_query = f"select count(*) from game where state = 'false' and server_id = {self.server_id}"
        self.cur.execute(win_query)
        games_won = self.cur.fetchall()
        self.cur.execute(lose_query)
        games_lost = self.cur.fetchall()
        total_score['crewmates'] = games_won[0][0]
        total_score['world'] = games_lost[0][0]
        self.cur.close()
        self.conn.close()

        return total_score

    def insert_new_server(self, message, webhook, region):
        """
        :param message:'Message' object to get information about server
        :param webhook: Webhook object from discord_bot
        :param region: Region from discord bot input
        :return: adding data to PSQL
        """
        route = regions.RegionDecider().get_route_by_region(region)
        query = "insert into servers " \
                "(server_name, admin_id, server_id, channel_id, webhook_id, webhook_token, region, route, last_update)" \
                " values ( %s, %s, %s, %s, %s, %s, %s, %s, now())"
        webhook_text = f'Added new server: \n{message.guild.name} ({message.channel_id})'
        service_webhook_sender.SendServiceWebhookMessage('new_server').send_notification('New server added', webhook_text)
        server_data = (message.guild.name,
                       message.user.id,
                       message.guild.id,
                       message.channel_id,
                       webhook.id,
                       webhook.token,
                       region,
                       route
                       )
        self.cur.execute(query, server_data)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def get_admin_id_by_server(self):

        self.cur.execute(f"select admin_id from servers where server_id = '{self.server_id}'")
        admin_id = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        return admin_id[0].split(',')

    def get_region_by_server_id(self):

        self.cur.execute(f"select region from servers where server_id = '{self.server_id}'")
        region = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        return region[0]

    def insert_new_summoner(self, puuid, nickname, discord_id, encrypt_id, summoner_lvl, message):
        """

        """
        webhook_text = f'{nickname} added to "{message.guild.name}" server'
        service_webhook_sender.SendServiceWebhookMessage('new_summoner').send_notification('New summoner added', webhook_text)
        query = "insert into crew " \
                "(puuid, nickname, discord_id, server_id, encrypt_id, summoner_level)" \
                " values ( %s, %s, %s, %s, %s, %s)"
        summoner_data = (puuid, nickname, discord_id, self.server_id, encrypt_id, summoner_lvl)
        try:
            self.cur.execute(query, summoner_data)
        except psycopg2.errors.UniqueViolation:
            return 1
        self.conn.commit()
        self.cur.close()
        self.conn.close()
        return 0

    def get_webhook_keys(self):
        self.cur.execute(f"select webhook_id, webhook_token from servers where server_id = '{self.server_id}'")
        keys = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return keys[0]

    def get_list_of_servers(self):
        """

        :return: List of all the servers from PSQL
        """

        list_of_servers = []

        self.cur.execute(f"select server_id from servers")
        servers = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for server in servers:
            list_of_servers.append(server[0])

        return list_of_servers

    def get_number_of_summoners(self):
        """
        :return: number of summoners at this server
        """
        query_for_number_of_summoner = f"select count(*) from crew where server_id = {self.server_id}"
        self.cur.execute(query_for_number_of_summoner)
        number_of_summoner = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return number_of_summoner[0][0]

    def get_possible_number_of_summoners(self):
        """
        :return: number of summoners at this server
        """
        query_for_possible_number_of_summoner = f"select number_of_summoners from" \
                                                f" servers where server_id = {self.server_id}"
        self.cur.execute(query_for_possible_number_of_summoner)
        possible_number_of_summoner = self.cur.fetchall()
        self.cur.close()
        self.conn.close()
        return possible_number_of_summoner[0][0]

    def get_number_of_servers(self):
        """
        :return: number of servers
        """
        query_for_number_of_servers = f"select count(*) from servers"
        self.cur.execute(query_for_number_of_servers)
        number_of_servers = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return number_of_servers[0][0]

    def get_possible_number_of_servers(self):
        """
        :return: possible number of servers
        """
        query_for_possible_number_of_servers = f"select possible_number_of_servers from" \
                                                f" settings"
        self.cur.execute(query_for_possible_number_of_servers)
        possible_number_of_servers = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return possible_number_of_servers[0][0]

    def add_another_admin(self, current_admin, new_admin):
        admin_string = ''
        if new_admin in current_admin:
            return 1
        for admin in current_admin:
            admin_string += f'{admin},'
        admin_string += new_admin
        query = f"update servers SET admin_id = '{admin_string}' where server_id = {self.server_id}"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def update_admins_list(self, list_of_admins):
        admin_string = ''
        for admin in list_of_admins:
            admin_string += f',{admin}'
        admin_string = admin_string[1:]
        query = f"update servers SET admin_id = '{admin_string}' where server_id = {self.server_id}"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def get_list_of_summoners(self):
        list_of_summoners = []

        self.cur.execute(f"select nickname from crew where server_id = {self.server_id}")
        summoners = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for summoner in summoners:
            list_of_summoners.append(summoner[0])

        return list_of_summoners

    def delete_summoner(self, nickname):
        query = f"delete from crew where nickname = '{nickname}'"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def update_activity(self):
        query = f"update servers set last_update = now() where server_id = '{self.server_id}'"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def delete_server_data(self):
        delete_servers_query = f"delete from servers where server_id = '{self.server_id}'"
        delete_play_query = f"delete from play where puuid in (select puuid from crew where server_id = '{self.server_id}')"
        delete_game_query = f"delete from game where server_id = '{self.server_id}'"
        delete_crew_query = f"delete from crew where server_id = '{self.server_id}'"
        delete_casino_query = f"delete from casino where server_id = '{self.server_id}'"

        try:
            list_of_queries = [delete_play_query, delete_game_query, delete_crew_query, delete_servers_query, delete_casino_query]
            for query in list_of_queries:
                self.cur.execute(query)
                self.conn.commit()
            return 0
        except: 
            return 1
        finally:
            self.cur.close()
            self.conn.close()
        
    def get_list_of_servers_to_delete(self):

        list_of_dead_servers = []
        query_to_select_old_servers = "select server_id from servers where now() - last_update > interval '2 month';"
        self.cur.execute(query_to_select_old_servers)
        dead_servers = self.cur.fetchall()
        self.cur.close()
        self.conn.close()
        # convert list of tuples to list
        for server in dead_servers:
            list_of_dead_servers.append(server[0])
        print(list_of_dead_servers)
        return list_of_dead_servers
    
    def get_list_of_encrypted_ids(self):
        """

        :return: List of all crewmates by encrypted ID
        """

        list_of_crewmates = []

        self.cur.execute(f"select encrypt_id from crew where server_id = {self.server_id}")
        members = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for member in members:
            list_of_crewmates.append(member[0])

        return list_of_crewmates

    def get_champ_name_by_id(self, id):
        
        self.cur.execute(f"select name from champions where id = '{id}'")
        champ_name = self.cur.fetchone()
        self.cur.close()
        self.conn.close()

        try:
            champ_name = champ_name[0]
        except TypeError:
            champ_name = False

        return champ_name
    
    def add_champ_by_id(self, id, name):

        query = f"insert into champions (id, name) values ({str(id)}, '{name}')"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()




##########CASINO###########
    def get_server_points(self):
        query_to_select_users_and_points = f"select discord_id, points from casino where server_id = '{self.server_id}' order by points desc;"
        self.cur.execute(query_to_select_users_and_points)
        points = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        return points


    def get_list_of_gambling_servers(self):
        """

        :return: List of all the servers with gambling enabled from PSQL
        """

        list_of_servers = []

        self.cur.execute(f"select server_id from servers where gambling_enabled;")
        servers = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for server in servers:
            list_of_servers.append(server[0])

        return list_of_servers
    
    def get_list_of_gambled_games(self):
        """

        :return: List of all the viewed games from PSQL
        """

        list_of_gambled_games = []

        self.cur.execute(f"select game_id from casino_bets where server_id = {self.server_id} and initial")
        games = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for game in games:
            list_of_gambled_games.append(game[0])

        return list_of_gambled_games
    
    def insert_initial_bet(self, game_id):
        """

        """
        query = "insert into casino_bets " \
                "(game_id, server_id, created_on, initial)" \
                " values ( %s, %s, %s, %s)"
        gamble_id = (game_id, self.server_id, 'NOW()', True)
        try:
            self.cur.execute(query, gamble_id)
        except psycopg2.errors.UniqueViolation:
            return 1
        self.conn.commit()
        self.cur.close()
        self.conn.close()
        return 0
    
    def get_gamblers_points(self, discord_id):

        query_for_gambler_points = f"select points from casino where discord_id = {str(discord_id)} and server_id = {str(self.server_id)}"
        self.cur.execute(query_for_gambler_points)
        points = self.cur.fetchall()

        try: 
            points = points[0][0]
        except IndexError:
            self.insert_gambler_to_server(discord_id)
            return [1000, 2]
        
        self.cur.close()
        self.conn.close()

        return [points, 0]
    

    def insert_gambler_to_server(self, discord_id):
        query = "insert into casino (discord_id, points, creation_time, server_id) values ( %s, %s, %s, %s)"
        values = (discord_id, 1000, 'NOW()', self.server_id)

        self.cur.execute(query, values)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    
    def give_1000_if_gambler_on_0(self, discord_id):
        query = f'update casino SET points = 1000 where discord_id = {str(discord_id)} and server_id = {str(self.server_id)}'

        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()


    def get_active_bets(self):
        list_of_active_bets = []

        self.cur.execute(f"select game_id from casino_bets where server_id = {self.server_id} and now() - created_on < interval '7 minutes' and initial;")
        game_ids = self.cur.fetchall()
        self.cur.close()
        self.conn.close()

        # convert list of tuples to list
        for game in game_ids:
            list_of_active_bets.append(game[0])

        return list_of_active_bets        
    
    def insert_bet(self, game_id, points, discord_id, prediction):

        query = "insert into casino_bets (game_id, points, discord_id, result_to_be, initial, created_on, server_id) values ( %s, %s, %s, %s, %s, %s, %s)"
        values = (game_id, points, discord_id, prediction, False, 'NOW()', self.server_id)

        self.cur.execute(query, values)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def update_bet(self, points, discord_id, game_id):
        query = f"update casino_bets SET points = {str(points)} where discord_id = {str(discord_id)} and server_id = {str(self.server_id)} and game_id = '{game_id}'"

        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()
    
    def get_current_bet(self, discord_id, game_id):
        self.cur.execute(f"select points, result_to_be from casino_bets where discord_id = {str(discord_id)} and server_id = {str(self.server_id)} and game_id = '{game_id}'  and not initial;")
        # try:
        current_bet = self.cur.fetchall()
        if len(current_bet) == 0:
            return [0,0]
        self.cur.close()
        self.conn.close()

        return current_bet[0]
    
    def update_points(self, discord_id, total_points, delta=0, points=0):
        points_to_be = total_points + delta - points
        query = f"update casino SET points = {str(points_to_be)} where discord_id = {str(discord_id)} and server_id = {str(self.server_id)}"

        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()
    
    def get_bets_result(self, game_id):
        self.cur.execute(f"select points, discord_id, result_to_be from casino_bets where server_id = {str(self.server_id)} and game_id = '{game_id}' and not initial;")
        current_state = self.cur.fetchall()
        total_result = {'total_on_win': 0,
                        'total_on_lose': 0,
                        'bets_on_win': 0,
                        'bets_on_lose': 0,
                        'total_bets': 0,
                        'total_points': 0,
                        'participants': {}}
        for bet in current_state:
            points = bet[0]
            gambler = bet[1]
            result = bet[2]
            if result:
                total_result['total_on_win'] += points
                total_result['bets_on_win'] += 1
            else:
                total_result['total_on_lose'] += points
                total_result['bets_on_lose'] += 1
            
            total_result['total_bets'] += 1
            total_result['total_points'] += points
            total_result['participants'][gambler] = {}
            
            total_result['participants'][gambler]['bet_amount'] = points
            total_result['participants'][gambler]['result_predicted'] = result

        return total_result
    

    def delete_old_bets(self):
        query = "delete from casino_bets where now() - created_on > interval '24 hours';"
        self.cur.execute(query)
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    


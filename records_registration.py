import postgres_requests
import yaml

with open('stats.yaml', 'r') as file:
    template_with_stats = yaml.safe_load(file)


def get_top_stats(server_id):
    """
    If we have an empty DB, there will be the Index Error, so we fill response with fillers

    getting response from PSQL with all possible stats
    :return: list of dicts with new records
    """
    for stat in template_with_stats:
        if template_with_stats[stat]['record']:
            try:
                top_value_from_psql = postgres_requests.PostgresRequest(server_id).get_stat_by_plays(
                    stat,
                    template_with_stats[stat]['per_minute'],
                    limit=1
                )
                template_with_stats[stat]['record_value'] = top_value_from_psql[0][-1]
                template_with_stats[stat]['record_holder'] = top_value_from_psql[0][0]
                template_with_stats[stat]['record_champion'] = top_value_from_psql[0][1]
            except IndexError:
                template_with_stats[stat]['record_value'] = 0
                template_with_stats[stat]['record_holder'] = 'Top_player'
                template_with_stats[stat]['record_champion'] = 'Top_champion'

    return template_with_stats


def compare_stats_after_game(old_stats, new_stats):
    """

    :param old_stats: stats from previous request
    :param new_stats: stats from recent request
    :return: dict with changes of TOP scorers
    """
    dict_with_records = {}
    for stat in new_stats:
        if new_stats[stat]['record'] and new_stats[stat]['record_value'] > old_stats[stat]['record_value']:
            dict_with_records[stat] = [
                new_stats[stat]['record_holder'],
                new_stats[stat]['record_value'],
                new_stats[stat]['record_champion'],
            ]

    return dict_with_records


def get_top_five_overall(stat_name, server_id):
    """

    :param stat_name: stat name to get top5 values for it
    :param server_id: server_id to make correct selection
    :return: list of tuples with top5 stats from db
    """
    top_values = postgres_requests.PostgresRequest(server_id).get_stat_by_plays(
        stat_name,
        template_with_stats[stat_name]['per_minute'],
        limit=5
    )

    return top_values

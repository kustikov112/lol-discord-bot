import yaml
import google.generativeai as genai
import requests
import json


class GenAi:
    def __init__(self, crew, data, localization):
        with open('TOKEN.yaml', 'r') as f:
            tokens = yaml.safe_load(f)
        self.crew = crew
        self.data = data
        self.localization = localization
        self.token = tokens['gemini_ai_token']

        self.instruction = f"""
                            This is only players I care about. Lets call it Crew List - {self.crew}.
                            There is a metadata from the LoL match. Revise from the perspective of team where 2 or more of this puuid participated - {self.crew}.
                            Could you create 1 sentence about the result. Are they won because of enemie's mistakes or their performance? Or are they lost because enemy was stronger or because of self mistakes.
                            Also, give me a brief conclusion about who is from this puuid list - {self.crew} impacted the most on Win or Loss, please.
                            Consider not using puuid in response, but put summonerName in it.
                            Give some advices only for players in CrewList if they participated on how to improve performance.
                            If user not in this list - {self.crew} - don't make suggestions.
                            Use {self.localization} language in responce
                            """
    
    def generate_summary(self):    
        # print(f'\033[93m[DEBUG] \033[0m Setting up TOKEN')
        genai.configure(api_key=self.token)
        # print(f'\033[93m[DEBUG] \033[0m This is my prompt')
        # print(f'\033[93m[DEBUG] \033[0m {self.instruction}')
        model = genai.GenerativeModel(
            model_name="gemini-1.5-flash",
            system_instruction=self.instruction)
        # print(f'\033[93m[DEBUG] \033[0m Model is setup')
        # print(f'\033[93m[DEBUG] \033[0m Data from game')
        # print(f'\033[93m[DEBUG] \033[0m {self.data}')
        # print(f'\033[93m[DEBUG] \033[0m running generation')
        response = model.generate_content(self.data)
        # print(f'\033[93m[DEBUG] \033[0m Response status - {response}')
        # print(f'\033[93m[DEBUG] \033[0m Response text:')
        # print(f'\033[93m[DEBUG] \033[0m {response.text}')

        # print(response.text)

        return(response.text)
    
    def generate_summary_by_lambda_function(self):
        endpoint = 'https://13v2di9acd.execute-api.eu-central-1.amazonaws.com/prod/'
        headers = {
            "Content-Type": "application/json"
            }
        payload = {
            "data": self.data,
            "users": self.crew,
            "localization": self.localization
        }
        response = requests.get(endpoint, headers=headers, json=payload)
        resp_generated = json.loads(response.text)['body']
        text_generated = json.loads(resp_generated)

        return text_generated['candidates'][0]['content']['parts'][0]['text']
    
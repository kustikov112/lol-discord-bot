
--
-- Name: casino; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.casino (
    discord_id bigint NOT NULL,
    points bigint NOT NULL,
    creation_time timestamp without time zone NOT NULL,
    server_id bigint NOT NULL
);


ALTER TABLE public.casino OWNER TO lol_db;

--
-- Name: casino_bets; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.casino_bets (
    game_id character varying,
    points bigint,
    discord_id bigint,
    result_to_be boolean,
    initial boolean,
    created_on timestamp without time zone,
    server_id bigint
);


ALTER TABLE public.casino_bets OWNER TO lol_db;

--
-- Name: champions; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.champions (
    id integer,
    name character varying
);


ALTER TABLE public.champions OWNER TO lol_db;

--
-- Name: crew; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.crew (
    id integer NOT NULL,
    puuid character varying(90) NOT NULL,
    nickname character varying(50),
    discord_id character varying(50),
    server_id bigint,
    creation_time timestamp without time zone DEFAULT now(),
    encrypt_id character varying(150),
    summoner_level integer
);


ALTER TABLE public.crew OWNER TO lol_db;

--
-- Name: crew_id_seq; Type: SEQUENCE; Schema: public; Owner: lol_db
--

CREATE SEQUENCE public.crew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crew_id_seq OWNER TO lol_db;

--
-- Name: crew_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lol_db
--

ALTER SEQUENCE public.crew_id_seq OWNED BY public.crew.id;


--
-- Name: game; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.game (
    datetime timestamp without time zone,
    gameid character varying(15) NOT NULL,
    state character varying(10),
    duration integer,
    player1 integer,
    player2 integer,
    player3 integer,
    player4 integer,
    player5 integer,
    match_mode character varying(50),
    game_mode character varying(50),
    server_id bigint
);


ALTER TABLE public.game OWNER TO lol_db;

--
-- Name: play; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.play (
    playid integer,
    puuid character varying(90),
    goldearned integer,
    kills integer,
    assists integer,
    totalminionskilled integer,
    totaldamagedealttochampions integer,
    championname character varying(90),
    wardsplaced integer,
    deaths integer,
    largestmultikill integer,
    longesttimespentliving integer,
    totaldamagetaken integer,
    individualposition character varying(90),
    summonername character varying(90)
);


ALTER TABLE public.play OWNER TO lol_db;

--
-- Name: servers; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.servers (
    id integer NOT NULL,
    server_name character varying(150),
    admin_id character varying(1000),
    server_id bigint,
    channel_id bigint,
    webhook_id bigint,
    webhook_token character varying(150),
    region character varying(10),
    route character varying(20),
    number_of_summoners integer DEFAULT 5,
    addition_time timestamp without time zone DEFAULT now(),
    last_update timestamp without time zone,
    gambling_enabled boolean
);


ALTER TABLE public.servers OWNER TO lol_db;

--
-- Name: servers_id_seq; Type: SEQUENCE; Schema: public; Owner: lol_db
--

CREATE SEQUENCE public.servers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.servers_id_seq OWNER TO lol_db;

--
-- Name: servers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lol_db
--

ALTER SEQUENCE public.servers_id_seq OWNED BY public.servers.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: lol_db
--

CREATE TABLE public.settings (
    possible_number_of_servers bigint DEFAULT 10
);


ALTER TABLE public.settings OWNER TO lol_db;

--
-- Name: crew id; Type: DEFAULT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.crew ALTER COLUMN id SET DEFAULT nextval('public.crew_id_seq'::regclass);


--
-- Name: servers id; Type: DEFAULT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.servers ALTER COLUMN id SET DEFAULT nextval('public.servers_id_seq'::regclass);


--
-- Name: crew crew_nickname_key; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.crew
    ADD CONSTRAINT crew_nickname_key UNIQUE (nickname);


--
-- Name: crew crew_pkey; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.crew
    ADD CONSTRAINT crew_pkey PRIMARY KEY (puuid);


--
-- Name: game game_pkey; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT game_pkey PRIMARY KEY (gameid);


--
-- Name: servers servers_channel_id_key; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.servers
    ADD CONSTRAINT servers_channel_id_key UNIQUE (channel_id);


--
-- Name: servers servers_server_id_key; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.servers
    ADD CONSTRAINT servers_server_id_key UNIQUE (server_id);


--
-- Name: servers servers_webhook_id_key; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.servers
    ADD CONSTRAINT servers_webhook_id_key UNIQUE (webhook_id);


--
-- Name: servers servers_webhook_token_key; Type: CONSTRAINT; Schema: public; Owner: lol_db
--

ALTER TABLE ONLY public.servers
    ADD CONSTRAINT servers_webhook_token_key UNIQUE (webhook_token);